# Generate a KMS Key using Terraform

## Required ENV Variables

```sh
export AWS_ACCOUNT='[account-id]'
export AWS_USER='terraform'
export AWS_REGION="eu-central-1"

export AWS_ACCESS_KEY="******"
export AWS_SECRET_KEY="******"

export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY}"
export TF_VAR_aws_access_key="${AWS_ACCESS_KEY}"

export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_KEY}"
export TF_VAR_aws_secret_key="${AWS_SECRET_KEY}"

export AWS_URL="https://${AWS_ACCOUNT}.signin.aws.amazon.com/console"
echo -e "Loaded AWS ${AWS_ACCOUNT} ENV\nAWS_URL:\t${AWS_URL}\nAWS_USER:\t${AWS_USER}\nAWS_REGION:\t${AWS_REGION}"
```

## How to run

```sh
# Initialise Terraform
terraform init

# Plan changes
terraform plan -out tf.p

# Apply changes
terraform apply tf.p
```

## Important

You will need the terraform output, especially the `key_id`

## TF State

You should find ways to store the terraform state.
Using above configuration only, it will generate a local file `terraform.tfstate`.
Keep it safe!

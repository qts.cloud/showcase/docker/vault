variable "name" {
  type    = string
  default = "vault-unseal"
}

variable "deletion_window_in_days" {
  type    = number
  default = 30
}

resource "aws_kms_key" "this" {
  description             = var.name
  deletion_window_in_days = var.deletion_window_in_days
}

output "key_id" {
  value = aws_kms_key.this.key_id
}
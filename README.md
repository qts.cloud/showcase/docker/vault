## Setup Vault

* Prequisites:
  * basic linux knowledge
  * have docker already installed
  * have full access to the machine where docker is installed. We will refer to it as `192.168.1.102` from now on.
  * be able to do port-forwarding to `192.168.1.102`

* Setup Vault:
  * connect to `docker` machine and create your work directory

  ```sh
  ssh 192.168.1.102
  # Preparing structure
  mkdir -p qts && cd qts
  git clone https://gitlab.com/qts.cloud/showcase/docker/vault.git
  ```

  * Check Vault configuration file:

    ```json
    # qts/vault/config.json
    backend "file" {
      path = "/vault/storage"
    }

    seal "awskms" {
      disabled = true
    }

    seal "gcpckms" {
      disabled = true
    }

    ui = true

    listener "tcp" {
      address     = "0.0.0.0:8200"
      tls_disable = true
    }
    ```

  * Check Vault `docker-compose.yml`:

    ```yml
    # qts/vault/docker-compose.json
    version: "3"
    services:
      vault:
        image: ibacalu/vault:latest
        ports:
          - 8200:8200
        volumes:
          - ./storage:/vault/storage
          - ./config.hcl:/vault/config.hcl
        environment:
          VAULT_API_ADDR: http://127.0.0.1:8200
          # If you use AWS KMS, make sure to export below values
          AWS_REGION: ${AWS_REGION}
          AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID}
          AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY}
          VAULT_AWSKMS_SEAL_KEY_ID: ${VAULT_AWSKMS_SEAL_KEY_ID}
        command: server -config=/vault/config.hcl
        restart: always
        network_mode: host
        cap_add:
          - IPC_LOCK
    ```

  * Start Vault

    ```sh
    docker-compose up
    vault_1  | ==> Vault server configuration:
    vault_1  |
    vault_1  |              Api Address: http://127.0.0.1:8200
    vault_1  |                      Cgo: disabled
    vault_1  |          Cluster Address: https://127.0.0.1:8201
    vault_1  |               Go Version: go1.14.4
    vault_1  |               Listener 1: tcp (addr: "0.0.0.0:8200", cluster address: "0.0.0.0:8201", max_request_duration: "1m30s", max_request_size: "33554432", tls: "disabled")
    vault_1  |                Log Level: info
    vault_1  |                    Mlock: supported: true, enabled: true
    vault_1  |            Recovery Mode: false
    vault_1  |                  Storage: file
    vault_1  |                  Version: Vault v1.6.0
    ```

  * Initialize and unseal Vault:
    * login to `http://192.168.1.102:8200`
    * Fill `key shares` (1) and `key threshold` (1) and `initialize`
    * Download the keys!
      * if you miss downloading you can remove the `./storage` directory
      * `docker-compose down` && `docker-compose up`
    * Continue to unseal and enter the key you just downloaded.
    * Keep these files safe! You will need the token later.
    * Use the root token to authenticate

## Enable KMS Auto-Unseal

For this to work:

* make sure you update the config.hcl and set `disabled` to `false` in `awskms` seal
* Bootup the docker-compose
* configure locally the Vault credentials
* Migrate seal configuration

  ```sh
  vault operator unseal -migrate `my-unseal-key`
  ```
